import path from 'path';

export default class ElectronApp {
	constructor (backup, {app, BrowserWindow, ipc, dirname}) {
		this._backup = backup;
		this.app = app;
		this.deps = {BrowserWindow, ipc};
		this._dirname = dirname;
	}

	async launch () {
		return this._activateApp();
	}

	async _activateApp () {
		this._activateListeners();
		this.app.on('window-all-closed', this._onWindowAllClosed.bind(this));
		await this.app.whenReady();
		this._createWindow();
		this.app.on('activate', this._onActivate.bind(this));
	}

	_activateListeners () {
		const {ipc} = this.deps;
		ipc.on('app', (event, payload) => {
			if (!this._window) {
				return;
			}

			switch (payload.action) {
				case CONST.ACTIONS.MINIMISE: return this._onMinimise();
				case CONST.ACTIONS.CLOSE: return this._onClose();
				case CONST.ACTIONS.INIT: return this._onInit();
				case CONST.ACTIONS.DELETE: return this._onDelete(payload);
				case CONST.ACTIONS.SET_OPTIONS: return this._onChangeSettings(payload);
				case CONST.ACTIONS.BACKUP: return this._onBackup(payload);
				case CONST.ACTIONS.RESTORE: return this._onRestore(payload);
				case CONST.ACTIONS.UNDO: return this._onUndo(payload);
			}
		});
	}

	_createWindow () {
		const {BrowserWindow} = this.deps;
		this._window = new BrowserWindow({
			width: 890,
			height: 700,
			frame: false,
			transparent: true,
			resizable: false,
			webPreferences: {
				preload: path.join(this._dirname, 'src', 'preload.js')
			}
		});

		return this._window.loadFile(path.join(this._dirname, 'public', 'index.html'));
	}

	_onActivate () {
		const {BrowserWindow} = this.deps;
		if (!BrowserWindow.getAllWindows().length) {
			this._createWindow();
		}
	}

	async _onBackup ({world}) {
		let success;

		try {
			const operation = await this._backup.createBackupOperation(world);
			if (operation) {
				success = await operation.backup(false);
			} else {
				success = false;
			}
		} catch (e) {
			success = false;
		}

		this._window.webContents.send('app', {
			action: CONST.ACTIONS.BACKUP,
			worlds: await this._backup.getWorlds(),
			success
		});
	}

	_onClose () {
		this._window.close();
	}

	async _onChangeSettings ({options}={}) {
		const changed = Object.entries(options).reduce((acc, [k, v]) => {
			if (this._backup.options[k] !== v) {
				acc[k] = v;
			}
			return acc;
		}, {});

		if (!Object.keys(changed).length) {
			return;
		}

		await this._backup.config.write(changed);
		await this._backup.config.load();
		return this._onInit();
	}

	async _onDelete ({world, backup} = {}) {
		let success;

		try {
			const operation = await this._backup.createBackupOperation(world);
			if (operation) {
				success = await operation.delete(backup);
			} else {
				success = false;
			}
		} catch (e) {
			this._backup.logger.error(e);
			success = false;
		}

		this._window.webContents.send('app', {
			action: CONST.ACTIONS.DELETE,
			worlds: await this._backup.getWorlds(),
			success
		});
	}

	async _onInit () {
		this._window.webContents.send('app', {
			action: CONST.ACTIONS.INIT,
			options: this._backup.options,
			worlds: await this._backup.getWorlds(),
			languages: await this._backup.getLanguages()
		});
	}

	_onMinimise () {
		this._window.minimize();
	}

	async _onRestore ({world, backup} = {}) {
		let success;

		try {
			const operation = await this._backup.createBackupOperation(world);
			if (operation) {
				const n = operation.backups.indexOf(backup);
				if (n < 0) {
					this._backup.logger.error(`Unable to find '${backup}' in world '${world}'.`);
					success = false;
				} else {
					success = await operation.restore(n + 1);
				}
			} else {
				success = false;
			}
		} catch (e) {
			this._backup.logger.error(e);
			success = false;
		}

		this._window.webContents.send('app', {
			action: CONST.ACTIONS.RESTORE,
			worlds: await this._backup.getWorlds(),
			undo: backup,
			success
		});
	}

	async _onUndo ({world} = {}) {
		let success;

		try {
			const operation = await this._backup.createBackupOperation(world);
			if (operation) {
				success = await operation.undo();
			} else {
				success = false;
			}
		} catch (e) {
			this._backup.logger.error(e);
			success = false;
		}

		this._window.webContents.send('app', {
			action: CONST.ACTIONS.UNDO,
			worlds: await this._backup.getWorlds(),
			success
		});
	}

	_onWindowAllClosed () {
		if (process.platform !== 'darwin') {
			this.app.quit();
		}
	}
}
