import ElectronApp from './ElectronApp.class.mjs';
import Config from './config/Config.class.mjs';
import Translation from '../public/common/Translation.class.mjs';
import path from 'path';
import {constants as FS, promises as fs} from 'fs';
import createLogger from './util/logger.mjs';
import BackupOperation from './BackupOperation.class.mjs';
import '../public/common/constants.mjs';

export default class FoundryBackup {
	get formatter () {
		return CONST.FORMATTER;
	}

	get options () {
		return this.config.options;
	}

	async inititalise ({optionsFile, dirname, electron} = {}) {
		this._dirname = dirname;
		await this._configureOptions(optionsFile);
		await this._loadLanguages();
		this.logger = createLogger(this.options.logs);

		if (electron) {
			const app = new ElectronApp(this, {dirname, ...electron});
			await app.launch();
		}
	}

	async createBackupOperation (world) {
		const operation = new BackupOperation(this);
		if (!(await operation.withWorld(world))) {
			return false;
		}

		return operation;
	}

	/**
	 * @param {string} worldKey
	 * @returns {Promise<{backupDir: string, backups: string[]}>}
	 */
	async getBackups (worldKey) {
		const backupDir = path.join(this.options.backups, worldKey);
		let backups = [];

		try {
			/** @type {string[]} **/
			const contents = (await fs.readdir(backupDir)) || [];
			backups = contents.filter(f => f.endsWith('.bak'));
		} catch (e) {
			return {backupDir, backups};
		}

		return {backupDir, backups: backups.sort(FoundryBackup._backupComparator)};
	}

	async getLanguages () {
		let langs = [{en: 'English'}];
		try {
			const langDir = path.join(this._dirname, 'public', 'lang');
			const jsons = await fs.readdir(langDir);
			langs = await Promise.all(jsons.map(async f => {
				const contents = await fs.readFile(path.join(langDir, f), 'utf8');
				const json = JSON.parse(contents);
				const [lang] = f.split('.');
				return {[lang]: json.LanguageName || ''};
			}));
		} catch (e) {
			this.logger.error(e);
		}

		return langs;
	}

	async getWorlds () {
		const existingWorlds = path.join(this.options.foundry, 'Data', 'worlds');
		const backups = this.options.backups;
		const worlds = new Set();

		for (const dir of [existingWorlds, backups]) {
			try {
				(await fs.readdir(dir, {withFileTypes: true})).forEach(entry => {
					if (entry.isDirectory()) {
						worlds.add(entry.name);
					}
				});
			} catch (ignored) {}
		}

		return Promise.all(Array.from(worlds).map(async world => {
			return {
				name: world,
				backups: (await this.getBackups(world)).backups
			}
		}));
	}

	async _configureOptions (optionsFile) {
		this.config = new Config(optionsFile);

		try {
			await this.config.load();
		} catch (e) {
			console.error(e);
			process.exit();
		}
	}

	async _loadLanguages () {
		let json = path.join(this._dirname, 'public', 'lang', `${this.options.lang}.json`);
		try {
			await fs.access(json, FS.R_OK);
		} catch (e) {
			console.warn(`Failed to load ${json}, falling back to en.json...`);
			json = path.join(this._dirname, 'public', 'lang', 'en.json');
		}

		const strings = JSON.parse(await fs.readFile(json, 'utf8'));
		this.i18n = new Translation(strings);
	}

	static _backupComparator (a, b) {
		const [a1, a2] = a.split('.').map(n => parseInt(n));
		const [b1, b2] = b.split('.').map(n => parseInt(n));

		if (a1 === b1) {
			return a2 === b2 ? 0 : a2 > b2 ? -1 : 1;
		}

		return a1 > b1 ? -1 : 1;
	}
}
