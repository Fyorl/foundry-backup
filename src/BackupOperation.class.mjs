import path from 'path';
import {promises as fs, constants as FS, createWriteStream, createReadStream} from 'fs';
import os from 'os';
import tar from 'tar-fs';
import {createGunzip, createGzip} from 'zlib';
import {pipeline} from 'stream';
import {rename} from './util/util.mjs';

/**
 * @typedef {object} World
 * @property {string} title
 * @property {string} name
 */

export default class BackupOperation {
	get backups () {
		return this._backups || [];
	}

	constructor (foundryBackup) {
		this.options = foundryBackup.options;
		this.logger = foundryBackup.logger;
		this.i18n = foundryBackup.i18n;
		this._fb = foundryBackup;
	}

	async withWorld (world) {
		this._worldPath = await this._findWorld(world);
		this._existingWorld = !!this._worldPath;
		this._worldKey = world;
		this._backups = [];

		if (!this._existingWorld) {
			this.logger.warn(this.i18n.translate('FallbackSearch', world));
			const backupDir = path.join(this.options.backups, world);

			try {
				await fs.access(backupDir, FS.R_OK);
			} catch (e) {
				this.logger.error(e);
				return false;
			}

			this._worldPath = path.join(this.options.foundry, 'Data', 'worlds', world);
		}

		this._worldKey = path.basename(this._worldPath);
		const {backupDir, backups} = await this._fb.getBackups(this._worldKey);
		this._backupDir = backupDir;
		this._backups = backups;
		this._undo = await this._getUndo(backupDir);
		return true;
	}

	async backup (cleanup = true) {
		if (!(await this._makeBackup())) {
			return false;
		}

		if (!cleanup) {
			return true;
		}

		try {
			await this._cleanup();
		} catch (e) {
			this.logger.error(e);
			return false;
		}

		return true;
	}

	async delete (backup) {
		if (!this._backupDir || !this._backups.includes(backup)) {
			return false;
		}

		await fs.unlink(path.join(this._backupDir, backup));

		return true;
	}

	async list () {
		if (this._backups.length < 1) {
			return this.i18n.translate('NoBackups', this._worldKey);
		}

		const list = [this.i18n.translate('BackupsFor', this._worldKey)];

		for (let i = 0; i < this._backups.length; i++) {
			const backup = this._backups[i];
			const date = Date.fromBackupName(backup);
			const n = (i + 1).toString().padStart(4, ' ');
			const formatted = date ? this._fb.formatter.format(date) : 'INVALID';
			list.push(`${n}\t${formatted}`);
		}

		return list.join(os.EOL);
	}

	async restore (n) {
		if (typeof n === 'boolean') {
			n = 1;
		}

		return this._restore(n);
	}

	async undo () {
		if (!this._undo) {
			return false;
		}

		return this._restore(0, true);
	}

	async _cleanup () {
		const diff = this._backups.length - this.options.maxBackups;
		if (isNaN(diff) || diff < 1) {
			return;
		}

		const remove = this._backups.slice(diff * -1);
		await Promise.all(remove.map(bak => {
			const f = path.join(this._backupDir, bak);
			this.logger.info(this.i18n.translate('CleaningUp', f));
			return fs.unlink(f);
		}));
	}

	async _clearUndo () {
		try {
			const files = await fs.readdir(this._backupDir);
			const undo = files.filter(f => f.endsWith('.undo'));
			await Promise.all(undo.map(f => fs.unlink(path.join(this._backupDir, f))));
		} catch (e) {
			this.logger.error(e);
		}
	}

	async _findWorld (world) {
		const worldDir = path.join(this.options.foundry, 'Data', 'worlds');
		const worlds = (await fs.readdir(worldDir, {withFileTypes: true})).reduce((acc, dir) => {
			if (dir.isDirectory()) {
				acc.push(dir.name);
			}

			return acc;
		}, []);

		// If the world matches a folder name, we're done.
		if (worlds.includes(world)) {
			return path.join(worldDir, world);
		}

		// Otherwise search through all the world titles and see if there's a
		// match.
		/** @type {Promise<World>[]} */
		const promises = worlds.map(w => {
			const json = path.join(worldDir, w, 'world.json');
			return fs.readFile(json, 'utf8').then(JSON.parse);
		});

		const worldJSONs = await Promise.all(promises);
		const match = worldJSONs.find(json => json.title === world);

		if (match) {
			return path.join(worldDir, match.name);
		}

		this.logger.error(this.i18n.translate('WorldNotFound', world));
		return null;
	}

	/**
	 * @param {string} backupDir
	 * @returns {Promise<?string>}
	 * @private
	 */
	async _getUndo (backupDir) {
		try {
			const files = await fs.readdir(backupDir);
			return files.find(f => f.endsWith('.undo'));
		} catch (e) {
			this.logger.warn(e);
		}

		return null;
	}

	async _makeBackup (ext = 'bak') {
		let tmp;
		try {
			tmp = await this._makeTempDir();

			const tar = path.join(tmp, `${this._worldKey}.tar`);
			this.logger.info(`'${this._worldPath}' -> '${tar}'...`);
			await BackupOperation._createTar(this._worldPath, tar);

			const bak = path.join(tmp, `${this._worldKey}.bak`);
			this.logger.info(`'${tar}' -> '${bak}'...`);
			await BackupOperation._gzip(tar, bak);
			await fs.unlink(tar);

			const entry = await this._newBackup(ext);
			this.logger.info(`'${bak}' -> '${entry}...'`);
			await rename(bak, entry);
		} catch (e) {
			this.logger.error(e);
			return false;
		} finally {
			if (tmp) {
				await fs.rm(tmp, {recursive: true});
			}
		}

		return true;
	}

	async _makeTempDir () {
		return fs.mkdtemp(path.join(os.tmpdir(), 'foundry-backup-'));
	}

	async _newBackup (ext = 'bak') {
		const backupDir = path.join(this.options.backups, this._worldKey);
		try {
			await fs.mkdir(backupDir, {recursive: true});
		} catch (ignored) {}

		return path.join(backupDir, `${new Date().toBackupName()}.${ext}`);
	}

	async _restore (n, undo = false) {
		if (!undo && this._backups.length < n) {
			return this.i18n.translate('NoBackups', this._worldKey);
		}

		if (!undo) {
			await this._clearUndo();
			if (this._existingWorld && !(await this._makeBackup('undo'))) {
				return false;
			}
		}

		let tmp;
		const backup = undo ? this._undo : this._backups[n - 1];

		try {
			tmp = await this._makeTempDir();
			const bak = path.join(this._backupDir, backup);
			const tar = path.join(tmp, `${this._worldKey}.tar`);
			this.logger.info(`${bak} -> ${tar}...`);
			await BackupOperation._gunzip(bak, tar);

			if (this._existingWorld) {
				this.logger.info(this.i18n.translate('DeleteWorld', this._worldKey));
				await fs.rm(this._worldPath, {recursive: true});
			}

			this.logger.info(`${tar} -> ${this._worldPath}`);
			await BackupOperation._untar(tar, this._worldPath);
		} catch (e) {
			this.logger.error(e);
			return false;
		} finally {
			if (tmp) {
				await fs.rm(tmp, {recursive: true});
			}
		}

		if (undo) {
			await this._clearUndo();
		}

		const date = Date.fromBackupName(backup);
		return this.i18n.translate(
			'BackupRestored', this._worldKey, this._fb.formatter.format(date));
	}

	static async _compressionStream (src, dest, transform) {
		return new Promise((resolve, reject) => {
			const read = createReadStream(src);
			const write = createWriteStream(dest, {mode: 0o644});

			pipeline(read, transform, write, err => {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		});
	}

	static async _createTar (src, dest) {
		return new Promise((resolve, reject) => {
			if (src == null) {
				reject();
			}
			const stream = createWriteStream(dest, {mode: 0o644});
			stream.on('error', reject);
			stream.on('finish', () => resolve());
			tar.pack(src).pipe(stream);
		});
	}

	static async _gunzip (src, dest) {
		return this._compressionStream(src, dest, createGunzip());
	}

	static async _gzip (src, dest) {
		return this._compressionStream(src, dest, createGzip({level: 9}));
	}

	static async _untar (src, dest) {
		return new Promise((resolve, reject) => {
			const stream = tar.extract(dest);
			stream.on('error', reject);
			stream.on('finish', () => resolve());
			createReadStream(src).pipe(stream);
		});
	}
}
