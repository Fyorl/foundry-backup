const {contextBridge, ipcRenderer} = require('electron');

const validChannels = new Set(['app']);
contextBridge.exposeInMainWorld('ipc', {
	send: function (channel, data) {
		if (validChannels.has(channel)) {
			ipcRenderer.send(channel, data);
		}
	},

	receive: function (channel, fn) {
		if (validChannels.has(channel)) {
			ipcRenderer.on(channel, (event, ...args) => fn(...args));
		}
	}
});
