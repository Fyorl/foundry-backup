import path from 'path';
import {constants as FS, promises as fs} from 'fs';
import os from 'os';
import {override} from '../util/util.mjs';
import '../../public/common/primitives.mjs';

export default class Config {
	static _DEFAULT_OPTIONS = {
		lang: 'en',
		maxBackups: 10
	};

	constructor (optionsFile) {
		this._optionsFile = optionsFile;
	}

	async load () {
		const optionsFile = this._getOptionsFile();
		const optionsDir = path.dirname(optionsFile);
		const readWrite = FS.W_OK | FS.R_OK;

		try {
			await fs.access(optionsDir, readWrite);
		} catch (e) {
			await fs.mkdir(optionsDir, {recursive: true, mode: 0o755});
		}

		try {
			await fs.access(optionsFile, readWrite);
		} catch (e) {
			await this._create(optionsFile);
		}

		const options = JSON.parse(await fs.readFile(optionsFile, 'utf8'));
		const defaults = await this._determineDefaults();
		this.options = override(defaults, options);
	}

	async write (newOptions = {}) {
		const optionsFile = this._getOptionsFile();
		const options = JSON.parse(await fs.readFile(optionsFile, 'utf8'));
		const json = JSON.stringify(override(options, newOptions), null, '\t');
		return fs.writeFile(optionsFile, json, {encoding: 'utf8', mode: 0o644, flag: 'w'});
	}

	_create (optionsFile) {
		return fs.writeFile(optionsFile, JSON.stringify(Config._DEFAULT_OPTIONS, null, '\t'), {
			encoding: 'utf8',
			mode: 0o644,
			flag: 'w'
		});
	}

	async _determineDefaults () {
		const defaults = Config._DEFAULT_OPTIONS;
		const home = this._determineDataDir();
		defaults.logs = path.join(home, 'logs');
		defaults.backups = path.join(home, 'backups');

		const defaultFoundry = this._determineDataDir('FoundryVTT');
		const foundryConfig = path.join(defaultFoundry, 'Config', 'options.json');

		let foundryOptions;
		try {
			foundryOptions = JSON.parse(await fs.readFile(foundryConfig, 'utf8'));
		} catch (ignored) {}

		defaults.foundry =
			process.env.FOUNDRY_VTT_DATA_PATH || foundryOptions?.['dataPath'] || defaultFoundry;

		return defaults;
	}

	_determineDataDir (app = 'foundry-backup') {
		const homedir = os.homedir();

		switch (process.platform) {
			case 'linux':
				return path.join(
					process.env.XDG_DATA_DIR || path.join(homedir, '.local', 'share'), app);
			case 'darwin':
				return path.join(homedir, 'Library', 'Application Support', app);
			case 'win32':
				return path.join(
					process.env.LOCALAPPDATA || path.join(homedir, 'AppData', 'Local'), app);
		}
	}

	_getOptionsFile () {
		return this._optionsFile || path.join(this._determineDataDir(), 'options.json');
	}
}
