import winston from 'winston';
import path from 'path';

export default function createLogger (logDir) {
	const {createLogger, format, transports} = winston;
	return createLogger({
		level: 'debug',
		format: format.combine(
			format.timestamp({format: 'YYYY-MM-dd HH:mm:ss'}),
			format.errors({stack: true}),
			format.json()
		),
		transports: [
			new transports.File({filename: path.join(logDir, 'debug.log')}),
			new transports.File({filename: path.join(logDir, 'error.log'), level: 'error'}),
			new transports.Console({
				format: format.combine(
					format.colorize(),
					format.printf(log =>
						`[${log.timestamp}] [${log.level}] ${log.message}`
						+ (log.stack ? `\n${log.stack}` : ''))
				)
			})
		]
	});
};
