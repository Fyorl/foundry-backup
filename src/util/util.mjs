import mv from 'mv';

export function override (a, b) {
	// Override a with b
	const obj = {};

	for (const [k, v] of Object.entries(a)) {
		obj[k] = v;
	}

	for (const [k, v] of Object.entries(b)) {
		if (obj[k] instanceof Object && v instanceof Object) {
			obj[k] = override(obj[k], v);
		} else {
			obj[k] = v;
		}
	}

	return obj;
}

export function rename (src, dest) {
	return new Promise((resolve, reject) => {
		mv(src, dest, function (err) {
			if (err != null) {
				return reject(err);
			}
			resolve();
		});
	});
}
