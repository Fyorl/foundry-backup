#!/usr/bin/env node
const yargs = require('yargs/yargs');
const {hideBin} = require('yargs/helpers');

const parser = yargs(hideBin(process.argv));
parser.usage(
	'Usage: $0 --world [world] [--options [options.json]] [--restore [n]] [--list] [--undo]\n'
	+ 'Make sure foundry is not running when using this tool otherwise it will behave '
	+ 'unexpectedly.');

parser.alias('h', 'help');

parser.option('w', {
	alias: 'world',
	demandOption: true,
	type: 'string',
	describe: 'The data path name of your world (i.e. its folder name), '
		+ 'or the world title (case-sensitive).'
});

parser.option('r', {
	alias: 'restore',
	type: 'boolean|number',
	describe: 'Restore the nth most recent backup of the world. Passing this option without a '
		+ 'number will default to n=1, i.e. restore the most recent backup.'
});

parser.option('l', {
	alias: 'list',
	type: 'boolean',
	describe: 'List the available backups for the world.'
});

parser.option('u', {
	alias: 'undo',
	type: 'boolean',
	describe: 'Undo the last --restore operation.'
});

parser.option('o', {
	alias: 'options',
	type: 'string',
	describe: 'By default, settings are stored in a location appropriate for your OS. If you '
		+ 'wish to provide your own settings, you can pass the path to your options.json here.'
});

parser.example('$0 --world my-world', "Create a backup of the 'my-world' folder.");
parser.example('$0 --world "My World"', "Find the world called 'My World' and back it up.");
parser.example('$0 --world my-world --list', "List all the current backups of 'my-world'.");
parser.example('$0 --world my-world --undo', 'Undo the last --restore operation.');
parser.example(
	'$0 --world my-world --restore',
	'Overwrite your existing world with the most recent backup.');
parser.example(
	'$0 --world my-world --restore 5',
	"Restore the 5th most recent backup of 'my-world'");

const args = parser.parse();

const run = async function () {
	const FoundryBackup = (await import('./src/FoundryBackup.class.mjs')).default;
	const backup = new FoundryBackup();
	await backup.inititalise({optionsFile: args.options, dirname: __dirname});
	const operation = await backup.createBackupOperation(args.world);

	if (!operation) {
		return;
	}

	if (!args.restore && !args.list && !args.undo) {
		await operation.backup();
		console.log('Done.');
	}

	const operations = new Set(['restore', 'list', 'undo']);
	if (Object.keys(args).filter(arg => operations.has(arg)).length > 1) {
		console.error('Only one of --restore, --list, or --undo can be passed.');
		process.exit(1);
	}

	if (args.restore) {
		const out = await operation.restore(args.restore);
		if (typeof out === 'string') {
			console.log(out);
		}
	} else if (args.list) {
		const list = await operation.list();
		console.log(list);
	} else if (args.undo) {
		const out = await operation.undo();
		if (typeof out === 'string') {
			console.log('Undone.');
		}
	}
};

try {
	return run();
} catch (e) {
	console.error(e);
}
