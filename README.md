# Foundry Backup
A [Foundry VTT](https://foundryvtt.com) backup utility.

## Usage
Foundry Backup can be used from the command-line, and also comes with a GUI.

### GUI
![](gui.webp "GUI example")

To use the GUI, download a build appropriate for your OS from [here](https://bytestruct.org/foundry-backup). Information beyond here is for advanced use of the utility. If you're happy with using it through the GUI, you do not need to go any further.

### CLI
The CLI requires at least [node.js](https://nodejs.org/en/) version 14. Download the latest version of this repository from [here](https://bitbucket.org/Fyorl/foundry-backup/get/master.zip) and unzip it to its own directory. Change into that directory and then run the following to install dependencies:

```
$ npm install --only=prod
```

Once those are installed, you can run it from the command-line with:

```
$ npm run cli -- --help
```

This will show you a list of the available command-line options. Alternatively you may symlink the `foundry-backup.js` file somewhere on your `$PATH` and run it with:

```
$ foundry-backup.js --help
```

```
Usage: foundry-backup.js --world [world] [--options [options.json]] [--restore
[n]] [--list] [--undo]
Make sure foundry is not running when using this tool otherwise it will behave
unexpectedly.

Options:
      --version  Show version number                                   [boolean]
  -w, --world    The data path name of your world (i.e. its folder name), or the
                 world title (case-sensitive).               [string] [required]
  -r, --restore  Restore the nth most recent backup of the world. Passing this
                 option without a number will default to n=1, i.e. restore the
                 most recent backup.
  -l, --list     List the available backups for the world.             [boolean]
  -u, --undo     Undo the last --restore operation.                    [boolean]
  -o, --options  By default, settings are stored in a location appropriate for
                 your OS. If you wish to provide your own settings, you can pass
                 the path to your options.json here.                    [string]
  -h, --help     Show help                                             [boolean]

Examples:
  foundry-backup.js --world my-world        Create a backup of the 'my-world'
                                            folder.
  foundry-backup.js --world "My World"      Find the world called 'My World' and
                                            back it up.
  foundry-backup.js --world my-world        List all the current backups of
  --list                                    'my-world'.
  foundry-backup.js --world my-world        Undo the last --restore operation.
  --undo
  foundry-backup.js --world my-world        Overwrite your existing world with
  --restore                                 the most recent backup.
  foundry-backup.js --world my-world        Restore the 5th most recent backup
  --restore 5                               of 'my-world'
```

In general it is recommended to use the folder name of your world, rather than its title. In cases where your world has been deleted and you wish to restore from backup, foundry-backup.js has no way of looking up the title of your world, but can still restore from the folder name.

## Building
Building the project yourself can be done with:

```
$ npm install
```

... to install dependencies, then:

```
$ npm start
```

... to launch it. Building an electron package for a given platform can be done with:

```
$ npm run make -- --platform linux
```

Available platforms are: `linux`, `darwin`, and `win32`.

## License
All source code (`*.html`, `*.css`, `*.js`, `*.json`) is copyright 2021 Kim Mantas.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

A copy of this license is available in the `LICENSE` file, or can alternatively be found here: [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html).

The font Roboto found in the `public/fonts` directory is used and redistributed under the terms of the [Apache License v2](http://www.apache.org/licenses/LICENSE-2.0). A copy of this license is available in the `public/fonts/LICENSE-2.0.txt` file.

All images found in the `public/img` directory are copyright 2021 [mikiko](https://mikiko.art) and are used and redistributed under the terms of [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
