Date.prototype.toBackupName = function () {
	const YYYY = this.getUTCFullYear();
	const MM = (this.getUTCMonth() + 1).toString().padStart(2, '0');
	const dd = this.getUTCDate().toString().padStart(2, '0');
	const HH = this.getUTCHours() * 3600;
	const mm = this.getUTCMinutes() * 60;
	const ss = this.getUTCSeconds();
	return `${YYYY}${MM}${dd}.${HH + mm + ss}`;
};

Date.fromBackupName = function (name) {
	let [date, seconds] = name.split('.');
	seconds = parseInt(seconds);

	if (date.length !== 8 || isNaN(seconds)) {
		return null;
	}

	const YYYY = parseInt(date.substr(0, 4));
	const MM = parseInt(date.substr(4, 2)) - 1;
	const dd = parseInt(date.substr(6));

	if ([YYYY, MM, dd].some(isNaN)) {
		return null;
	}

	const HH = Math.floor(seconds / 3600);
	const mm = Math.floor((seconds % 3600) / 60);
	const ss = (seconds % 3600) % 60;

	return new Date(Date.UTC(YYYY, MM, dd, HH, mm, ss));
};
