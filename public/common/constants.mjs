globalThis.CONST = Object.freeze({
	ACTIONS: {
		CLOSE: 0, MINIMISE: 1, INIT: 2, DELETE: 3, SET_OPTIONS: 4, BACKUP: 5, RESTORE: 6, UNDO: 7
	},

	FORMATTER: Intl.DateTimeFormat([], {dateStyle: 'medium', timeStyle: 'medium'})
});
