export default class Translation {
	constructor (strings) {
		this._strings = strings;
	}

	translate (key, ...args) {
		let string = this._strings[key];
		if (!string) {
			return key;
		}

		if (args.length) {
			const type = typeof args[0];
			if (!['string', 'number'].includes(type)) {
				args = args[0];
			}

			for (const key in args) {
				string = string.replace(new RegExp(`\\{${key}\\}`, 'gi'), args[key]);
			}
		}

		return string;
	}
}
