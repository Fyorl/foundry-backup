import Translation from '../common/Translation.class.mjs';

/**
 * @typedef {object} IPC
 * @property {IPC~receive} receive
 * @property {IPC~send} send
 */

/**
 * @typedef {object} BackupWorld
 * @property {string} name
 * @property {string[]} backups
 */

/**
 * @typedef {object} IPC~payload
 * @property {number} [action]
 * @property {object} [options]
 * @property {BackupWorld[]} [worlds]
 * @property {string} [world]
 * @property {string} [backup]
 * @property {boolean} [success]
 */

/**
 * @callback IPC~receive
 * @param {string} channel
 * @param {IPC~onReceive} onReceive
 */

/**
 * @callback IPC~send
 * @param {string} channel
 * @param {IPC~payload} payload
 */

/**
 * @callback IPC~onReceive
 * @param {IPC~payload} payload
 */

/**
 * @type {IPC}
 */
const ipc = window.ipc;

class Application {
	constructor () {
		this._activateCoreListeners();
		ipc.send('app', {action: CONST.ACTIONS.INIT});
	}

	_activateListeners () {
		$('#worlds li').addEventListener('click', this._onClickWorld.bind(this));
		$('.btn-backup').addEventListener('click', this._onBackup.bind(this));
		$('.btn-undo').addEventListener('click', this._onUndo.bind(this));
		$('.btn-delete').addEventListener('click', evt =>
			this._promptBeforeOperation(evt, 'Delete'));
		$('.btn-restore').addEventListener('click', evt =>
			this._promptBeforeOperation(evt, 'Restore'));
	}

	_activateCoreListeners () {
		ipc.receive('app', payload => {
			switch (payload.action) {
				case CONST.ACTIONS.INIT: return this._onInit(payload);
				case CONST.ACTIONS.DELETE: return this._onResponse('Delete', payload);
				case CONST.ACTIONS.BACKUP: return this._onResponse('Backup', payload);
				case CONST.ACTIONS.RESTORE: return this._onRestore(payload);
				case CONST.ACTIONS.UNDO: return this._onUndoResponse(payload);
			}
		});

		$('#close').addEventListener('click', () => ipc.send('app', {action: CONST.ACTIONS.CLOSE}));
		$('#minimise').addEventListener('click', () =>
			ipc.send('app', {action: CONST.ACTIONS.MINIMISE}));

		$('#open-settings').addEventListener('click', this._onOpenSettings.bind(this));
		$('#close-settings').addEventListener('click', this._onCloseSettings.bind(this));
		$('#settings select, #settings input')
			.addEventListener('change', this._onChangeSettings.bind(this));

		$('.btn-save').addEventListener('click', this._onSaveSettings.bind(this));
	}

	_createButton (type) {
		const lc = type.toLowerCase();
		const btn = document.createElement('button');
		btn.setAttribute('type', 'button');
		btn.classList.add('btn', `btn-${lc}`);
		btn.innerText = BUTTON_MAP[lc];

		if (this._i18n) {
			btn.setAttribute('title', this._i18n.translate(type));
		}

		return btn;
	}

	_getDialog () {
		const dialog = document.getElementById('dialog');
		const ok = dialog.querySelector('.btn-ok');
		ok.innerText = this._i18n.translate('OK');
		return dialog;
	}

	_getPrompt () {
		const prompt = document.getElementById('prompt');
		const confirm = prompt.querySelector('.btn-confirm');
		const deny = prompt.querySelector('.btn-deny');
		confirm.innerText = this._i18n.translate('Confirm');
		deny.innerText = this._i18n.translate('Deny');
		return prompt;
	}

	_onBackup (evt) {
		const world = evt.currentTarget.closest('[data-world-name]').dataset.worldName;
		ipc.send('app', {action: CONST.ACTIONS.BACKUP, world});
	}

	_onChangeSettings () {
		if (this._settingsDirty) {
			return;
		}

		this._settingsDirty = true;
		this._render();
	}

	_onClickWorld (evt) {
		this._world = evt.currentTarget.dataset.worldName;
		this._render();
	}

	_onCloseSettings () {
		this._settingsOpen = false;
		this._render();
	}

	_promptBeforeOperation (evt, operation) {
		const backup = evt.currentTarget.closest('[data-backup]').dataset.backup;
		const prompt = this._getPrompt();

		prompt.querySelector('p').innerText =
			this._i18n.translate(
				`${operation}Confirm`,
				this._world,
				CONST.FORMATTER.format(Date.fromBackupName(backup)));

		prompt.onclose = () => {
			if (prompt.returnValue === 'deny') {
				return;
			}

			const action = operation.toUpperCase();
			ipc.send('app', {action: CONST.ACTIONS[action], world: this._world, backup});
		};

		prompt.showModal();
	}

	/**
	 * @param {IPC~payload} payload
	 * @returns {Promise<void>}
	 * @private
	 */
	_onInit ({options, worlds, languages}) {
		this._settingsOpen = false;
		this._settingsDirty = false;
		this._options = options;
		this._worlds = this._sortWorlds(worlds);
		this._settings = {languages};
		this._render();
		return this._loadLanguages();
	}

	_onOpenSettings () {
		this._settingsOpen = true;
		this._render();
	}

	_onResponse (operation, {success, worlds}) {
		if (!success) {
			const dialog = this._getDialog();
			dialog.querySelector('p').innerText = this._i18n.translate(`${operation}Failed`);
			dialog.showModal();
			return;
		}

		this._worlds = worlds;
		this._render();
	}

	_onRestore ({worlds, undo, success}) {
		if (success) {
			this._undo = undo;
		}

		return this._onResponse('Restore', {success, worlds});
	}

	_onSaveSettings () {
		const form = $('#settings form').unwrap();
		const options = Array.from(form.elements).reduce((acc, el) => {
			acc[el.name] = el.value;
			return acc;
		}, {});
		ipc.send('app', {action: CONST.ACTIONS.SET_OPTIONS, options});
	}

	_onUndo () {
		ipc.send('app', {action: CONST.ACTIONS.UNDO, world: this._world});
	}

	_onUndoResponse ({worlds, success}) {
		if (success) {
			this._undo = null;
		}

		return this._onResponse('Undo', {success, worlds});
	}

	async _loadLanguages () {
		try {
			this._i18n = await this._loadLanguage(this._options.lang);
		} catch (e) {
			console.warn(`Failed to load ${this._options.lang}.json. Falling back to en.json.`);
			try {
				this._i18n = await this._loadLanguage('en');
			} catch (e) {
				console.error('Failed to load en.json.', e);
				this._i18n = new Translation({});
			}
		}

		$('[data-i18n]').forEach(el => {
			const i18n = this._i18n.translate(el.dataset.i18n);
			let text = Array.from(el.childNodes).find(node => node.nodeType === Node.TEXT_NODE);
			if (!text) {
				text = document.createTextNode('');
				el.prepend(text);
			}
			text.textContent = i18n;
		});

		$('.btn-undo').forEach(btn => btn.setAttribute('title', this._i18n.translate('Undo')));
		$('.btn-backup').forEach(btn => btn.setAttribute('title', this._i18n.translate('Backup')));
		$('.btn-delete').forEach(btn => btn.setAttribute('title', this._i18n.translate('Delete')));
		$('.btn-restore').forEach(btn =>
			btn.setAttribute('title', this._i18n.translate('Restore')));
	}

	async _loadLanguage (lang) {
		const res = await fetch(`lang/${lang}.json`);
		const json = await res.json();
		return new Translation(json);
	}

	_render () {
		this._renderWorlds();
		this._renderBackups();
		this._renderSettings();
		this._activateListeners();
	}

	_renderBackups () {
		if (!this._world) {
			return;
		}

		const list = $('#backups');
		list.empty();

		/** @type {string[]} */
		const backups = this._worlds.find(world => world.name === this._world)?.backups || [];

		backups.forEach(backup => {
			const li = document.createElement('li');
			const text =
				document.createTextNode(CONST.FORMATTER.format(Date.fromBackupName(backup)));
			const buttons = document.createElement('div');
			const restoreButton = this._createButton('Restore');
			const deleteButton = this._createButton('Delete');

			if (this._undo === backup) {
				const undoButton = this._createButton('Undo');
				buttons.appendChild(undoButton);
			}

			buttons.classList.add('buttons');
			buttons.appendChild(restoreButton);
			buttons.appendChild(deleteButton);
			li.dataset.backup = backup;
			li.appendChild(text);
			li.appendChild(buttons);
			list.appendChild(li);
		});
	}

	_renderSettings () {
		if (this._settingsOpen) {
			$('#contents').style.display = 'none';
			$('#settings').style.display = 'block';
			$('#close-settings').style.display = 'inline-flex';
		} else {
			$('#contents').style.display = 'grid';
			$('#settings').style.display = 'none';
			$('#close-settings').style.display = 'none';
		}

		if (this._settingsDirty) {
			$('.btn-save').style.visibility = 'visible';
		} else {
			$('.btn-save').style.visibility = 'hidden';
		}

		const form = $('#settings form').unwrap();
		Array.from(form.elements).forEach(el => {
			if (el.name === 'lang' || el.value.length) {
				return;
			}
			el.value = this._options[el.name];
		});

		const language = $('[name="lang"]');
		language.empty();

		const languages = this._settings.languages.sort((a, b) => {
			const [[, lang1]] = Object.entries(a);
			const [[, lang2]] = Object.entries(b);
			return lang1.toLowerCase() > lang2.toLowerCase() ? 1 : -1;
		});

		language.innerHTML = languages.map(lang => {
			const [[key, name]] = Object.entries(lang);
			const selected = this._options.lang === key ? 'selected' : '';
			return `<option value="${key}" ${selected}>${name}</option>`;
		}).join('');
	}

	_renderWorlds () {
		const worlds = $('#worlds');
		worlds.empty();

		this._worlds.forEach(world => {
			const li = document.createElement('li');
			const text = document.createTextNode(world.name);
			const backupButton = this._createButton('Backup');
			li.dataset.worldName = world.name;
			li.appendChild(text);
			li.appendChild(backupButton);
			worlds.appendChild(li);

			if (world.name === this._world) {
				li.classList.add('active');
			}
		});
	}

	/**
	 * @param {BackupWorld[]} worlds
	 * @returns {BackupWorld[]}
	 * @private
	 */
	_sortWorlds (worlds) {
		return worlds.sort((a, b) => a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1);
	}
}

window.$ = selector => new Proxy(document.querySelectorAll(selector), {
	get: function (elements, prop) {
		if (prop === 'empty') {
			return function () {
				elements.forEach(element =>
					Array.from(element.children).forEach(child => child.remove()));
			}
		}

		if (prop === 'unwrap') {
			return function () {
				if (!elements.length) {
					return null;
				} else if (elements.length === 1) {
					return elements[0];
				}
				return elements;
			}
		}

		if (typeof elements[prop] === 'function') {
			return function (...args) {
				return elements[prop](...args);
			};
		}

		try {
			if (typeof Element.prototype[prop] === 'function') {
				return function (...args) {
					elements.forEach(element => element[prop](...args));
				}
			}
		} catch (ignored) {}

		if (!elements.length) {
			return;
		}

		return elements[0][prop];
	},

	set: function (elements, prop, value) {
		if (!elements.length) {
			return;
		}

		elements[0][prop] = value;
		return true;
	}
});

const BUTTON_MAP = {
	backup: '⬇',
	delete: '×',
	restore: '⬆',
	undo: '⮪'
};

window.addEventListener('DOMContentLoaded', () => new Application());
