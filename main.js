const {app, BrowserWindow, ipcMain} = require('electron');

(async function () {
	const FoundryBackup = (await import('./src/FoundryBackup.class.mjs')).default;
	const backup = new FoundryBackup();
	await backup.inititalise({dirname: __dirname, electron: {app, BrowserWindow, ipc: ipcMain}});
})();
